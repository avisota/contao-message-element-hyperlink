<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:18:27+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['embed']['0']     = 'Link einbetten';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['embed']['1']     = 'Benutzen Sie den Platzhalter "%s" um den Link in einem Text einzubetten (beispielsweise <em>Für weitere Informationen besuchen Sie bitte %s</em>).';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['linkTitle']['0'] = 'Linktitel';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['linkTitle']['1'] = 'Der Linktitel wird anstelle der Ziel-URL angezeigt werden.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['link_legend']    = 'Hyperlink-Einstellungen';
