<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-message-element-hyperlink
 * @license    LGPL-3.0+
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['orm_avisota_message_content']['linkTitle'] = array(
    'Link title',
    'The link title will be displayed instead of the target URL.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['embed']     = array(
    'Embed the link',
    'Use the wildcard "%s" to embed the link in a phrase (e.g. <em>For more information please visit %s</em>).'
);

/**
 * Legends
 */
$GLOBALS['TL_LANG']['orm_avisota_message_content']['link_legend'] = 'Hyperlink settings';
