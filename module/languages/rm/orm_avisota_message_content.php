<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T16:03:57+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['embed']['0']     = 'Includer la colliaziun';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['embed']['1']     = 'Utilisescha la wildcard "%s" per includer la collaziun en ina construcziun (p.ex. <em>Per ulteriuras infurmaziuns pos ti visitar %s</em>). ';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['linkTitle']['0'] = 'Titel da la colliaziun';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['linkTitle']['1'] = 'Il titel da la colliaziun vegn mussà empè da l\'URL da destinaziun.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['link_legend']    = 'Configuraziun da la colliaziun';
